# Thème Drupal Biblissima

Thème utilisé actuellement pour le site projet.biblissima.fr, créé initialement pour Drupal 9 lors de la migration du site depuis Drupal 7.

Il est basé sur [Barrio](https://www.drupal.org/project/bootstrap_barrio) (Drupal 8/9 - Bootstrap 4/5) et le sous-thème [SASS Barrio Starter Kit](https://www.drupal.org/project/bootstrap_sass). Les deux sont gérés via Composer.


## Installation

Le thème de base Barrio et le sous-thème SASS Starter Kit sont installés via Composer : `composer require 'drupal/<themename>'`

- Installation de Barrio : https://www.drupal.org/docs/8/themes/barrio-bootstrap-4-drupal-89-theme/bootstrap-barrio-installation/installation
- Installation de SASS Barrio Starter Kit et création du sous-thème : https://www.drupal.org/docs/contributed-themes/bootstrap-45-barrio-sass-starter-kit/installation


## Configuration

### Librairie Bootstrap

Bootstrap est installé localement via Composer (dans `/vendor`) lors de l'installation du thème parent Barrio. Il est aussi possible d'utiliser Bootstrap via un CDN mais le choix de la version locale a été privilégié.

Ainsi la version locale de Bootstrap est déclarée dans le fichier de configuration du thème `biblissima_barrio.info.yml` :

```yaml
libraries:
  - biblissima_barrio/bootstrap
```

**NB** : du fait de l'utilisation du sous-thème SASS Barrio Starter Kit, les fichiers Bootstrap installés par Barrio ne sont pas utilisés par le sous-thème Biblissima. Ainsi Bootstrap est géré directement via npm au niveau du sous-thème, et les fichiers CSS et JS sont copiés dans leurs répertoires respectifs lors de la compilation via Gulp (cf. `gulpfile.js`).

Ainsi dans `biblissima_barrio.libraries.yml` la ligne pointant vers `/libraries` est commentée et seuls les fichiers présents dans le `/js` du sous-thème sont utilisés. Le CSS de Bootstrap est désactivé car il est lui aussi directement compilé et concaténé par Gulp (dans `style.css` et `style.min.css`) :

```yaml
bootstrap:
  js:
    #/libraries/bootstrap/dist/js/bootstrap.bundle.min.js: {}
    js/popper.min.js: {}
    js/bootstrap.min.js: {}
  css:
    component:
      #/libraries/bootstrap/dist/css/bootstrap.min.css: {}
      /libraries/bootstrap-icons-1.5.0/bootstrap-icons.css: {}
```

### Compilation CSS/JS

La compilation des fichiers Sass (dans `scss`) et JS se fait via Gulp (cf. `gulpfile.js`), à partir du répertoire du sous-thème `biblissima_barrio` :

```shell
gulp
```
